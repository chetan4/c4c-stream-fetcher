 var appConfig = require('./../config.js')
 var Wreck = require('wreck');
 var Stream = require('stream');
 var https = require('https');
 var zlib = require('zlib');
 var Unzip = require('./unzip.js');
 var events = require('events');
 var eventEmitter = new events.EventEmitter();

 var auditsEmitter = require('./audit').auditEmitter();

 /*
    Tweet Fetcher opens an HTTP connection to Gnip.
    1. This is an always on connection.
    2. It streams data (tweets) to a Redis List when tweets match the rulesets.
    3. It unzips the payload and adds the data to redis - Refer to unzip.js
    4. It uses HTTP Basic Auth for authentication.
    5. It logs all activites by emit 'audit' events. - Refer audit.js
  */


 var TweetFetcher = function(){
     var self = this;

     this.run = function(){
         var method = appConfig.gnipStreamingApi.method;
         var path = appConfig.gnipStreamingApi.path;
         var authString = appConfig.gnipStreamingApi.authString;
         var hostname = appConfig.gnipStreamingApi.hostname;
         var port = appConfig.gnipStreamingApi.port;

         var options = {
             path: path,
             hostname: hostname,
             port: port,
             headers: { "Authorization": 'Basic ' + authString},
             agent: new https.Agent({keepAlive: false})
         };

         var request = null;

         var callback = function(res){
             console.log("Response status Code:", res.statusCode);

             if(res.statusCode == 200){                 
                 res.pipe(Unzip.gunzip);
             }

             if (res.statusCode == 429){
                 auditsEmitter.emit('audit', "Rate Limit Reached", (new Date()).toString());
             }

             res.on('connect', function(res, socket, head){
                 console.log("Connected to Gnip...")
             });

             /*
                If an error occurs in the connection
                1. abort current request
                2. retry and stay connected.
              */
             res.on('error', function(err){
                 request.abort();
                 self.run()
             })
         };

         console.log("Connecting to GNIP...");
         Unzip.activateCallbacks();
         request = https.get(options, callback);

     };

     this.auditLog = function(){

     }
 };


 module.exports = TweetFetcher;