var zlib =  require('zlib');
var redis = require('./redis');
var config = require('./../config');

var apiConfig = config.api.v1;
var events = require('events');
var eventEmitter = new events.EventEmitter();
var auditsEmitter = require('./audit').auditEmitter();
var nonEmptyStringTerminatingWithLineBreak = new RegExp(/^(.)+(\r\n)$/)
var emptyStringTerminatingWithLineBreak = new RegExp(/^[\s]*(\r\n)+$/)

var Unzip = {
    gunzip: zlib.createGunzip(),

    activateCallbacks: function(){
        var dataBuffer = '';

        this.gunzip.on('data', function(data){            
            var emptyStringCheck = data.toString().match(emptyStringTerminatingWithLineBreak)
            var nonEmptyStringTerminatingWithLineBreakCheck = data.toString().match(nonEmptyStringTerminatingWithLineBreak) 

            if (emptyStringCheck == null){
                console.log(data.toString());

                if(nonEmptyStringTerminatingWithLineBreakCheck != null) {
                    dataBuffer += data;
                    
                    var passData = dataBuffer;
                    redis.lpush(apiConfig.newTweetStreamChannel, passData.toString(), function (status) {
                        auditsEmitter.emit('audit', 'Data pushed to redis', (new Date()).toString())                        
                    })

                    dataBuffer = '';                    

                }else{
                    dataBuffer += data;
                }
            }else {
                console.log('White Space');
                console.log(data.toString());
                auditsEmitter.emit('audit', 'Whitespace canned', (new Date()).toString());

            }

        })

        this.gunzip.on('end', function(data){
            console.log(data);
        });
    }
}







module.exports = Unzip;