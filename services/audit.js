var events = require('events');
var auditsEmitter = new events.EventEmitter();
var Audit = {
    activateAudits: function() {
        auditsEmitter.on('audit', function (data, time) {
            console.log('----------------------------------')
            console.log("Data", data);
            console.log("Time", time);
            console.log('----------------------------------')
        })
    },

    auditEmitter: function(){
        return auditsEmitter;
    }
};

module.exports = Audit;
