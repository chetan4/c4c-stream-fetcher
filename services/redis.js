var redisConfig = require('./../config.js').redis
var env = (process.env.NODE_ENV == undefined) ? process.env.NODE_ENV : 'development';
var redisEnvConfig = redisConfig[env];
var redis = require('redis');
var events = require('events');
var eventEmitter = new events.EventEmitter();
var auditsEmitter = require('./audit').auditEmitter();


var redisClient = null;

var Client = function(){
    var self = this;

    this.initialize = function(){
        redisClient =  redis.createClient({host: redisEnvConfig.hostname,
            port: redisEnvConfig.port})

        self.activateCallbacks();
        return redisClient;
    };

    this.activateCallbacks = function(){
        redisClient.on('error', function(err){
            console.log('Redis connection failure');
            auditsEmitter.emit('audit', 'Redis connection failure', (new Date()).toString());
            throw(err);
        });
    }

};

if((redisClient == null) || (!redisClient.connected)){
   redisClient = new Client().initialize();
    eventEmitter.emit("audit", "Redis Client initialized");
}



module.exports = redisClient;