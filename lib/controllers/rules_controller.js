var Joi = require('joi');
var https = require('https');
var apiConfig = require('./../../config.js');
var env = (process.env.NODE_ENV) ? undefined : 'development';
var rulesConfig = apiConfig.gnipRulesApi;
var hostname = rulesConfig.hostname;
var port = rulesConfig.port;
var path = rulesConfig.path;
var authString = apiConfig.gnipStreamingApi.authString;
var url = apiConfig.gnipRulesApi.url;
var zlib = require('zlib');
var wreck = require('wreck');
var querystring = require('querystring');
var auditEmitter = require('./../../services/audit').auditEmitter();



/*
* Fetches all the rules
* */
var renderError = function(reply, payload){
    reply(payload).type('application/type')
}

var renderOk = function(reply, payload){
   reply(payload).type('application/type');
}

var badRequest = function(msg){
    return {
        status: 403,
        replyStatus: 'badRequest',
        message: msg.toString()
    };
};

var Ok = function(payload){
    return {
        status: 200,
        replyStatus: 'Ok',
        payload: payload
    }
}

 module.exports.index = {
  description: 'Returns all the rules',
  handler: function(request, reply){
      var options = {
          path: path,
          url: url,
          hostname: hostname,
          port: port,
          headers: { "Authorization": 'Basic ' + authString},
          agent: new https.Agent({keepAlive: false})
      };


      wreck.get(options.url, options, function(err, res, payload){
          if (res.statusCode == 200){
              var parsedPayload = JSON.parse(payload);

              reply({status: 200,
                     payload: parsedPayload
                    }).type('application/json');
          }else {
              reply({
                  status: 403,
                  requestStatus: 'Bad Request',
                  payload: null
              }).type('application/json')
          }

      })
  }

};

/*
    Parameters

    {
        "condition": "\"@super_sid AND microservices\"",
        "user_id": 22

    }

    OR

     {

        "condition": "user:super_sid",
        "user_id": 22

     }


 */
module.exports.create = {
    description: 'Adds a new rule',
    handler: function(request, reply){
        var options = {
            path: path,
            url: url,
            hostname: hostname,
            port: port,
            headers: { "Authorization": 'Basic ' + authString},
            agent: new https.Agent({keepAlive: false}),
            method: 'POST'
        };

        var validationSchema = Joi.object().keys({
            condition: Joi.string().required(),
            user_id: Joi.string().required()
        })

        Joi.validate(request.payload, validationSchema, function(err, value){
            if(err){
                renderError(reply, badRequest(err))
            }else{
                var post_data = {
                    rules: [{
                        value: request.payload.condition,
                        tag: request.payload.user_id
                    }],
                    sent: (new Date()).toISOString()
                }

                var post_request = https.request(options, function(res){
                    var buffer = ''
                    res.on('data', function(chunk){
                        buffer += chunk;
                    });

                    res.on('end', function(){
                        auditEmitter.emit('audit',
                                          "Rules Added " + JSON.stringify(post_data),
                                           (new Date()).toString()
                                          )
                        renderOk(reply, Ok(buffer));
                    })

                    res.on('error', function(err){
                        renderError(reply, badRequest(err))
                    })
                });

                post_request.write(JSON.stringify(post_data));
                post_request.end();
            }
        })



    }
}

module.exports.destroy = {
    description: "Deletes a rule",
    handler: function(request, reply){
        var options = {
            path: path,
            url: url,
            hostname: hostname,
            port: port,
            headers: { "Authorization": 'Basic ' + authString},
            agent: new https.Agent({keepAlive: false}),
            method: 'DELETE'
        };

        var validationSchema = Joi.object().keys({
            condition: Joi.string().required(),
            user_id: Joi.string().required()
        })

        Joi.validate(request.payload, validationSchema, function(err, value){
            if(err){
                renderError(reply, badRequest(err))
            }else{
                var post_data = {
                    rules: [{
                        value: request.payload.condition,
                        tag: request.payload.user_id
                    }],
                    sent: (new Date()).toISOString()
                }

                var post_request = https.request(options, function(res){
                    var buffer = ''
                    res.on('data', function(chunk){
                        buffer += chunk;
                    });

                    res.on('end', function(){
                        auditEmitter.emit('audit',
                                           "Rules Deleted: " + JSON.stringify(post_data),
                                          (new Date()).toString()
                                        )
                        renderOk(reply, Ok(buffer));
                    })

                    res.on('error', function(err){
                        renderError(reply, badRequest(err))
                    })
                });

                post_request.write(JSON.stringify(post_data));
                post_request.end();
            }
        })
    }
};