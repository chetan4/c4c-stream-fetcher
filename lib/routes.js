var RulesController = require('./controllers/rules_controller.js')

module.exports = [
    { path: '/api/v1/rules', method: 'GET', config: RulesController.index },
    { path: '/api/v1/rules', method: 'POST', config: RulesController.create},
    { path: '/api/v1/rules', method: 'DELETE', config: RulesController.destroy }
]