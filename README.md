### Installation

1. Install node -v 0.10.32
2. Install npm -v 1.4.28
3. Clone the repository and run npm install
4. Run nvm 0.10.32 index.js

The application requires a Redis server.

### Todos

1. OnRestart of the server fetch tweets missed
   during the disconnected phase.
2. Log audit events to a redis List or a file and
   display on a page
3. Build Rules 


### Documentation of APIs


# Installation

1. Install node -v 0.10.32
2. Install npm -v 1.4.28
3. Clone the repository and run npm install
4. Run nvm 0.10.32 index.js

The application requires a Redis server.

### Todos

1. OnRestart of the server fetch tweets missed
   during the disconnected phase.
2. Log audit events to a redis List or a file and
   display on a page
3. Build Rules 


### Documentation of APIs

## 1. Request GNIP API Rules

This endpoint allows you to fetch all rules that are currently active on GNIP

**Request**
```
GET /api/v1/rules
```

**Response**

The response's `tag` attribute returns the users following that keyword or user.

```
{
    "status": 200,
    "payload": {
        "rules": [
            {
                "value": "from:ashedryden",
                "tag": "22, 32, 12"
            }
        ]
    }
}
```


## 2. Add a new Rule

Rules can be added to indicate what kind of tweets you want to the stream to return. The rules are of the following types

* Get notified when someone tweets.
* Track a keyword
* Track responses to a tweet
* Track phrases.

Lets look at how to generate the request body JSON for each of these

**2.1. Get notified when `@chadfowler` tweets**

**Parameters**
```
  <condition> Describes the condition to track
  <user_id> The user requesting this tracking
 ```
 Example
 ```
   {
      condition: "from:chadfowler",
      user_id: "22"
    }
```



 The `user_id` field can be comma separated when 2 or more users are tracking the same user/keyword. This needs to be managed at the `user-facing-web-application`
 
 ```
   {
      condition: "from:super_sid",
      user_id: "22,32,43"
    }
 ```
 
 **2.2. Track a keyword**

 Example
 ```
   {
      condition: "microservices",
      user_id: "22"
    }
```

**2.2.1. Track multiple words in a sentence**

This would find the words 'microservices', 'Groovy' used in a tweet. 

Example
 ```
   {
      condition: "microservices with Groovy",
      user_id: "22"
    }
```

**2.2.2. Track a phrase in a sentence**

This would find the exact phrase 'microservices with Groovy' in a tweet.

Example

 ```
   {
      condition: "\"microservices with Groovy\"",
      user_id: "22"
    }
```

**2.3. Response to a tweet** 

```
   {
      condition: "in_reply_to_tweet_id:12393761252",
      user_id: "22"
    }
```


### Delete

Deletes a rule from the GNIP filter stream. 

```
 DELETE /api/v1/rules
```

**Request**

```
 {
      condition: "in_reply_to_tweet_id:12393761252",
      user_id: "22"
    }
```

**Response**

```
  {
  {"status":200,"replyStatus":"Ok","payload":""}
  }
```



