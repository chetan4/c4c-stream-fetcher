var Buffer = require('buffer').Buffer;

var config = {
    gnipStreamingApi:{
        hostname: 'stream.gnip.com',
        port: 443,
        url: 'https://stream.gnip.com:443/accounts/IdyllicSoftware/publishers/twitter/streams/track/prod.json',
        username: 'jparekh@idyllic-software.com',
        password: 'd7111978',
        method: 'GET',
        path: '/accounts/IdyllicSoftware/publishers/twitter/streams/track/prod.json'
    },

    gnipRulesApi: {
        url: 'https://api.gnip.com:443/accounts/IdyllicSoftware/publishers/twitter/streams/track/prod/rules.json',
        path: '/accounts/IdyllicSoftware/publishers/twitter/streams/track/prod/rules.json',
        username: 'jparekh@idyllic-software.com',
        password: 'd7111978',
        method: 'GET',
        hostname: 'api.gnip.com',
        port: 443
    },

    redis: {
        development: {
            hostname: 'localhost',
            port: 6379
        },

        production: {
            hostname: 'localhost',
            port: 6379
        }

    },

    api: {
        v1: {
            newTweetStreamChannel: '/tweets'
        }
    }
};

var base64AuthString = new Buffer(config.gnipStreamingApi.username + ":" +
            config.gnipStreamingApi.password
          ).toString('base64')

config.gnipStreamingApi.authString = base64AuthString;

module.exports = config