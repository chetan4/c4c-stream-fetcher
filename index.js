var Hapi = require('hapi');
var Wreck = require('wreck');
var server = new Hapi.Server();
var TweetFetcher = require('./services/tweet_fetcher')
var auditor = require('./services/audit');
var Routes = require('./lib');


server.connection({ port: 3000 });

if ((process.env.NODE_ENV == undefined) || (process.env.NODE_ENV == null))
    process.env.NODE_ENV = 'development';

console.log(process.env.NODE_ENV);

server.register({
    register: Routes,
    options: {}
}, function(err){
    if(err){
        console.log('Plugin could not be registered');
        console.log(err);
        throw(err);
    }
})


server.start(function () {
    auditor.activateAudits();
    console.log('Server running at:', server.info.uri);
    console.log('Launching Tweet Fetcher...');
    var tweetFetcher = new TweetFetcher();
    tweetFetcher.run();
});